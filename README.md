# Browser App

The feature of opening web links is used in almost every Android application. In this task, you will practice implementing an application that will load web links. To perform this task, you will need your knowledge of views and resources in Android.

This task can take you about 5 hours to complete.

Please be aware that the task status is mandatory.

## Task Description
Let's imagine that you need to create an app that will allow you to open web links as a part of the application functionality.

- The app should have an option of entering a web link in edit text and by tapping on the search button (a separate view), the page from the web link should be loaded.
- The app should have an action bar with the title of the current WebView page. If it is empty or not loaded, it should still show the app name "Browser app".
- The action bar should have the option of changing the color of the button. The following options are allowed: blue, green, violet. Each option is a separate menu option in the action bar. Blue is the default option.
- The search button should have the title "Search" and the text should have a floating hint "Web link".
- Finally, override the menu for a landscape layout. The following colors should be used for landscape layouts: brown, yellow, orange. Additionally, update menu titles using appropriate colors.
## Complete the Task
To perform the task, you should take the following steps:

1. Create an Activity with views to be able to load web links as a part of the app functionality.
2. Add a menu to change the color of the button that loads a website. Use three color options provided below:
 - for a portrait layout – blue, green, violet
 - for a landscape layout – brown, yellow, orange
3. Update the toolbar title after loading a web page.