package com.example.browserapp

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import com.example.browserapp.databinding.ActivityMainBinding
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import java.util.Locale


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupWebView()
        setupToolbar()

        binding.searchButton.setOnClickListener {
            val inputText = binding.textInputEditText.text.toString()
            binding.webView.loadUrl(inputText)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        binding.webView.apply {
            webViewClient = WebViewClient()
            settings.javaScriptEnabled = true
            webChromeClient = object : WebChromeClient() {
                override fun onReceivedTitle(view: WebView?, title: String?) {
                    if (title.isNullOrEmpty() || title == "about:blank"){
                        binding.toolbar.title = resources.getString(R.string.app_name)
                    } else {
                        binding.toolbar.title = title
                    }
                }
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.d(LOG, "menu orientation: ${newConfig.orientation}")
        updateMenu(newConfig.orientation)
    }

    private fun updateMenu(orientation: Int) {
        val menuResId = if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            R.menu.menu_portrait
        } else R.menu.menu_landscape
        Log.d(LOG, "menu: $menuResId")
        binding.toolbar.menu.clear()
        menuInflater.inflate(menuResId, binding.toolbar.menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.options_green -> changeButtonColor(Color.parseColor(COLOR_GREEN))
            R.id.options_blue -> changeButtonColor(Color.parseColor(COLOR_BLUE))
            R.id.options_violet -> changeButtonColor(Color.parseColor(COLOR_VIOLET))
            R.id.options_brown -> changeButtonColor(Color.parseColor(COLOR_BROWN))
            R.id.options_yellow -> changeButtonColor(Color.parseColor(COLOR_YELLOW))
            R.id.options_orange -> changeButtonColor(Color.parseColor(COLOR_ORANGE))
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_portrait, menu)
        return true
    }

    private fun changeButtonColor(parseColor: Int) {
        binding.searchButton.backgroundTintList = ColorStateList.valueOf(parseColor)
    }

    companion object {
        const val LOG = "MainActivity"
        const val COLOR_BLUE = "#0000FF"
        const val COLOR_GREEN = "#008000"
        const val COLOR_VIOLET = "#7F00FF"
        const val COLOR_BROWN = "#964B00"
        const val COLOR_YELLOW = "#FFFF00"
        const val COLOR_ORANGE = "#FFA500"
    }
}